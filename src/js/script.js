"use strict";
jQuery(document).ready(function($){
  $('.slide-introduce').owlCarousel({
    loop:true,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})
var owl = $('.slide-introduce');
owl.owlCarousel();
$('.customnext').click(function() {
    owl.trigger('next.owl.carousel');
})
$('.customprev').click(function() {
    owl.trigger('prev.owl.carousel', [300]);
})
   $('.slide-show').owlCarousel({
        items:1,
        loop:false,
        margin:10,
        URLhashListener:true,
        autoplayHoverPause:true,
        startPosition: 'URLHash'
    });
   $("a.tab").click(function(event){
   		$("a.tab").css({
	   		"background-color": "#eeeeee","color": "white",
	    });
	    $("a.tab").children('span').css({
	    	color: "#222222"
	    });
	    $("a.tab").children('i').css({
	    	color: "#eeeeee"
	    });
   		$(this).css({
   			"background-color": "#3f85ce"
   		});
   		$(this).children().css({
   			color: "white",
   			display: "inline-block"
   		});
   });

   $(document).click(function (e) {
    if (!containermenu.is(e.target) &&
        containermenu.has(e.target).length == 0) {
        //Đúng là bấm chuột ngoài menu
         var isopened =
            containermenu.find('.menu').css("display");

        //Ẩn menu đang mở
         if (isopened == 'block') {
             containermenu.find('.menu').slideToggle(500);
        }
    }
});

   $('#form-contact').validate({
    rules:{
      full_name:{
        required:true,
        maxlength:30,
      },
      phone_number:{
        required: true,
        minlength: 10,
        maxlength:11
      }
    },
    messages:{
      full_name:{
        required:"*Bạn chưa nhập dữ liệu"
      },
      phone_number:{
        required: "*Bạn chưa nhập dữ liệu",
        minlength: jQuery.validator.format("SĐT từ 10 -> 11 số"),
        maxlength: jQuery.validator.format("SĐT từ 10 -> 11 số")
      }
    }
  });

});

jQuery(document).ready(function(){

    var $listMenu = $('.menu');
        $('.click').click(function(){
            $('#burger').toggleClass('active');
            $('.wrapper').toggleClass('menu_open');
              if ($('#burger').hasClass('active')) {
             $listMenu.show();
                      setTimeout(function(){	
                        $listMenu.css('right','-15px');
                      }, 200);
                } else {
                $listMenu.css('right','-265px');
                setTimeout(function(){	
                $listMenu.hide();
              }, 500);
                }
              return false;
        });
    });
